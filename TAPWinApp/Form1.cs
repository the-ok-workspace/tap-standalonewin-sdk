﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;
using System.IO; 

using TAPWin;

namespace TAPWinApp
{
    public partial class Form1 : Form
    {

        private bool once;
        //Used for the timer
        /*private System.Windows.Forms.Timer tmr;*/
        private string startTime;
        private string TapName;
        private bool isRecording = false;
        private string recFileName;
        private int raw_reading_counter = 0;
        private bool isTapConnected = false;
        public Form1()
        {
            this.once = true;

            InitializeComponent();
            /*the following block is used to close the application after x minutes*/
/*            tmr = new System.Windows.Forms.Timer();
            tmr.Tick += delegate { this.Close(); };
            tmr.Interval = (int)TimeSpan.FromMinutes(1).TotalMilliseconds;
            tmr.Start();*/
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);
            if (this.once)
            {
                this.once = false;
                TAPManager.Instance.OnMoused += this.OnMoused;
                TAPManager.Instance.OnTapped += this.OnTapped;
                TAPManager.Instance.OnTapConnected += this.OnTapConnected;
                TAPManager.Instance.OnTapDisconnected += this.OnTapDisconnected;

                TAPManager.Instance.OnAirGestured += this.OnAirGestured;
                TAPManager.Instance.OnChangedAirGestureState += this.OnChangedAirGestureState;
                TAPManager.Instance.OnRawSensorDataReceieved += this.OnRawSensorDataReceieved;


                //              TAPManager.Instance.SetDefaultInputMode(TAPInputMode.Controller(), true);
                //              Task.Delay(5000);
                //TAPManager.Instance.SetDefaultInputMode(TAPInputMode.RawSensor(new RawSensorSensitivity()),true);

                TAPManager.Instance.Start();


            }


        }

        private void OnTapped(string identifier, int tapcode)
        {
            this.LogLine(identifier + " tapped " + tapcode.ToString());
        }

        private void OnTapConnected(string identifier, string name, int fw)
        {
            //this.LogLine(identifier + " connected. (" + name + ", fw " + fw.ToString() + ")");
            this.TapName = name;
            this.isTapConnected = true;

            toolStripStatusLabel1.Text =  "🔗" + name + " connected! Firmware :" + fw.ToString();
            textBoxConnected.BackColor = Color.MediumBlue; 



        }

        private void OnTapDisconnected(string identifier)
        {
            //this.LogLine(identifier + " disconnected.");
            toolStripStatusLabel1.Text = "🔌" + this.TapName + " disconnected.";
            this.isTapConnected = false;
            textBoxConnected.BackColor = Color.White;

            if (this.isRecording == true)
            {
                TAPManager.Instance.Deactivate();
                buttonRecStop.Text = "REC";
                buttonRecStop.BackgroundImage = Properties.Resources.record_icon;
                buttonRecStop.ForeColor = Color.Black;
                timerCalcFreq.Stop();
                this.isRecording = false;
            }



        }

        private void OnMoused(string identifier, int vx, int vy, bool isMouse)
        {
            if (isMouse)
            {
                this.LogLine(identifier + " moused (" + vx.ToString() + "," + vy.ToString() + ")");
            }

        }

        private void LogLine(string line)
        {
            this.Invoke((MethodInvoker)delegate
            {
                textBox1.AppendText(line + Environment.NewLine);

            });

        }

        private void VibrateButtonClick(object sender, EventArgs e)
        {
            TAPManager.Instance.Vibrate(new int[] { 100, 300, 100 });
        }

        private void OnAirGestured(string identifier, TAPAirGesture airGesture)
        {
            Console.WriteLine("AirGestured " + identifier + ", code: " + (int)airGesture);
        }

        private void OnChangedAirGestureState(string identifier, bool isInAirGestureState)
        {
            Console.WriteLine("Changed AirGesture State " + identifier + "isInAirGestureState: " + isInAirGestureState.ToString());
        }

        private async void OnRawSensorDataReceieved(string identifier, RawSensorData rsData)
        {
            /*this.LogLine(rsData.ToString());
            // RawSensorData has a timestamp, type and an array of points(x,y,z)
            if (rsData.type == RawSensorDataType.Device)
            {
                Point3 thumb = rsData.GetPoint(RawSensorData.indexof_DEV_THUMB);
                if (thumb != null)
                {
                    // thumb.x, thumb.y, thumb.z ...
                }
                // Etc.. use indexes: RawSensorData.indexof_DEV_THUMB, RawSensorData.indexof_DEV_INDEX, RawSensorData.indexof_DEV_MIDDLE, RawSensorData.indexof_DEV_RING, RawSensorData.indexof_DEV_PINKY
            }
            else if (rsData.type == RawSensorDataType.IMU)
            {
                Point3 gyro = rsData.GetPoint(RawSensorData.indexof_IMU_GYRO);
                if (gyro != null)
                {
                    // gyro.x, gyro.y, gyro.z ...
                }
                // Etc.. use indexes: RawSensorData.indexof_IMU_GYRO, RawSensorData.indexof_IMU_ACCELEROMETER
            }
*/
            // Please refer readme.md for more information


            //this.LogLine("Inside Raw Data Function");
            //this.LogLine(rsData.ToString());


            // RawSensorData has a timestamp, type and an array of points(x,y,z)
            /*            if (rsData.type == RawSensorDataType.Device)
                        {
                            //Point3 thumb = rsData.GetPoint(RawSensorData.indexof_DEV_THUMB);
                            Point3 thumb = rsData.GetPoint(RawSensorData.indexof_IMU_ACCELEROMETER);
                            Point3 index = rsData.GetPoint(RawSensorData.indexof_DEV_INDEX);
                            Point3 middle = rsData.GetPoint(RawSensorData.indexof_DEV_MIDDLE);
                            Point3 ring = rsData.GetPoint(RawSensorData.indexof_DEV_RING);
                            Point3 pinky = rsData.GetPoint(RawSensorData.indexof_DEV_PINKY);
                            if (thumb != null)
                            {
                                this.LogLine("----------------------------------------------");
                                this.LogLine("🕒 timestamp : " + rsData.timestamp.ToString());
                                this.LogLine("👍 thumb ACC : " + thumb.ToString());
                                this.LogLine("☝ index ACC : " + index.ToString());
                                this.LogLine("🖕 middle ACC : " + middle.ToString());
                                this.LogLine("💍 ring ACC : " + ring.ToString());
                                this.LogLine("🤙 pinky ACC : " + pinky.ToString());
                                this.LogLine("----------------------------------------------");

                                //Console.WriteLine("thumb " + thumb.ToString());

                                // thumb.x, thumb.y, thumb.z ...
                            }
                            // Etc.. use indexes: RawSensorData.indexof_DEV_THUMB, RawSensorData.indexof_DEV_INDEX, RawSensorData.indexof_DEV_MIDDLE, RawSensorData.indexof_DEV_RING, RawSensorData.indexof_DEV_PINKY
                        }

                        else if (rsData.type == RawSensorDataType.IMU)
                        {
                            Point3 gyro = rsData.GetPoint(RawSensorData.indexof_IMU_GYRO);
                            if (gyro != null)
                            {
                                this.LogLine("🕒 timestamp : " + rsData.timestamp.ToString());
                                this.LogLine("gyro X : " + gyro.x.ToString());
                                this.LogLine("gyro Y : " + gyro.y.ToString());
                                this.LogLine("gyro Z : " + gyro.z.ToString());
                                this.LogLine("----------------------------------------------");
                                string filename = @"D:\test.txt";
                                UnicodeEncoding unicoding = new UnicodeEncoding();
                                byte[] result = unicoding.GetBytes(gyro.x.ToString());
                                using (FileStream SourceStream = File.Open(filename, FileMode.OpenOrCreate))
                                {
                                    SourceStream.Seek(0, SeekOrigin.End);
                                    await SourceStream.WriteAsync(result, 0, result.Length);

                                }
                                // gyro.x, gyro.y, gyro.z ...
                            }
                            // Etc.. use indexes: RawSensorData.indexof_IMU_GYRO, RawSensorData.indexof_IMU_ACCELEROMETER
                        }
            */
            // Please refer readme.md for more information



            raw_reading_counter += 1; 
            string filename = this.recFileName;
            //string filename = "D:/" +  this.TapName + "_"+this.startTime + ".txt";
            UnicodeEncoding unicoding = new UnicodeEncoding();
            string data_string = rsData.ToString() + "\x0A";
            byte[] result = Encoding.ASCII.GetBytes(data_string);
            //byte[] result = unicoding.GetBytes(data_string);
            using (FileStream SourceStream = File.Open(filename, FileMode.OpenOrCreate))
            {
                SourceStream.Seek(0, SeekOrigin.End);
                SourceStream.Write(result, 0, result.Length);

            }
        }

        private void buttonRecStop_Click(object sender, EventArgs e)
        {
            if (this.isRecording)
            {
                TAPManager.Instance.Deactivate();
                TAPManager.Instance.SetTapInputMode(TAPInputMode.RawSensor(new RawSensorSensitivity()));

                this.isRecording = false;
                toolStripStatusLabel1.Text = "🔴 Recording stopped!";
                buttonRecStop.ForeColor = Color.Black;

                buttonRecStop.Text = "REC";
                buttonRecStop.BackgroundImage = Properties.Resources.record_icon;
                timerCalcFreq.Stop();

            }
            else
            {
                if (string.IsNullOrEmpty(this.recFileName)) { toolStripStatusLabel1.Text = " ⚠️ No file selected. Click the Browse Button to Select or Create a file."; }
                else if (this.isTapConnected == false) { toolStripStatusLabel1.Text = " ⚠️ No Tap connected. "; }
                    else
                        {
                    TAPManager.Instance.SetDefaultInputMode(TAPInputMode.RawSensor(new RawSensorSensitivity()), true);
                    TAPManager.Instance.Start();
                    TAPManager.Instance.Activate();


                    this.startTime = $"{DateTime.Now.ToString("yyyyMMddHHmmssfff")}";
                    this.isRecording = true;
                    buttonRecStop.ForeColor = Color.White;

                    buttonRecStop.Text = "STOP";
                    buttonRecStop.BackgroundImage = Properties.Resources.stop_icon;
                    toolStripStatusLabel1.Text = "🔴 Recording started!";
                    timerCalcFreq.Start();

                }

            }


        }


        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            this.recFileName = sender.ToString();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog v1 = new OpenFileDialog();
            v1.Filter = "Text files (*.txt){}|*.txt|All files (*.*)| *.*";
            v1.CheckFileExists = false;
            if (v1.ShowDialog() == DialogResult.OK)
            {
                textBox2.Text = Path.GetFileName(v1.FileName);
                textBox2.Text = v1.FileName;
                this.recFileName = v1.FileName;



            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            statusStrip1.BackColor = Color.LightGray;
        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolTipFileNameTextBox_Popup(object sender, PopupEventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //this.LogLine("tick!");
            this.LogLine(this.raw_reading_counter.ToString());
            textBoxDataRate.Text = this.raw_reading_counter.ToString();
            this.raw_reading_counter = 0;


        }

        private void textBoxDataRate_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void progressBarConnected_Click(object sender, EventArgs e)
        {

        }
    }
}
